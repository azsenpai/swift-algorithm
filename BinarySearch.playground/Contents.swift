
func linearSearch<T: Comparable>(_ array: [T], key: T) -> Int? {
    for i in 0 ..< array.count {
        if array[i] == key {
            return i
        }
    }

    return nil
}

func binarySearch<T: Comparable>(_ array: [T], key: T) -> Int? {
    guard !array.isEmpty else { return nil }

    var lower = 0
    var upper = array.count - 1

    while lower < upper {
        let middle = (lower + upper) / 2

        if array[middle] < key {
            lower = middle + 1
        } else {
            upper = middle
        }
    }

    return array[lower] == key ? lower : nil
}

let numbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67]

print(linearSearch(numbers, key: 43))
print(binarySearch(numbers, key: 43))
