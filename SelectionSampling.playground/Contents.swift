
func select<T>(from array: [T], count requested: Int) -> [T] {
    var result = [T]()

    var examined = 0
    var selected = 0

    while selected < requested {
        let r = Double.random(in: 0..<1)

        let leftToExamine = array.count - examined
        let leftToAdd = requested - selected

        if Double(leftToExamine) * r < Double(leftToAdd) {
            result.append(array[examined])
            selected += 1
        }

        examined += 1
    }

    return result
}

let input = [
    "there", "once", "was", "a", "man", "from", "nantucket",
    "who", "kept", "all", "of", "his", "cash", "in", "a", "bucket",
    "his", "daughter", "named", "nan",
    "ran", "off", "with", "a", "man",
    "and", "as", "for", "the", "bucket", "nan", "took", "it",
]

let output = select(from: input, count: 10)
