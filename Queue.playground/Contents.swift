
class Queue<T> {

    private var array = [T]()
    private var head = 0

    public var isEmpty: Bool {
        return count == 0
    }

    public var count: Int {
        return array.count - head
    }

    public func enqueue(_ newElement: T) {
        array.append(newElement)
    }

    public func dequeue() -> T? {
        guard !isEmpty else { return nil }

        let element = array[head]
        head += 1

        if array.count > 100 && Double(head) / Double(array.count) > 0.25 {
            array.removeFirst(head)
            head = 0
        }

        return element
    }

    public var front: T? {
        return isEmpty ? nil : array[head]
    }

}

let queue = Queue<String>()

queue.enqueue("First")
print("front: \(queue.front)")

queue.enqueue("Second")
print("front: \(queue.front)")

print("dequeue: \(queue.dequeue())")
print("dequeue: \(queue.dequeue())")
