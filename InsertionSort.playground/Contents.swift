
func insertionSort<T: Comparable>(_ array: [T]) -> [T] {
    var array = array

    for i in 1 ..< array.count {
        var j = i
        let element = array[i]

        while j > 0 && element < array[j-1] {
            array[j] = array[j-1]
            j -= 1
        }

        array[j] = element
    }

    return array
}

let list = [ 10, -1, 3, 9, 2, 27, 8, 5, 1, 3, 0, 26 ]
let sortedList = insertionSort(list)

print(sortedList)
