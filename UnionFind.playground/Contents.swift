
struct UnionFind<T: Hashable> {

    private var index = [T: Int]()
    private var parent = [Int]()
    private var size = [Int]()

    mutating func addSetWith(_ element: T) {
        index[element] = parent.count
        parent.append(parent.count)
        size.append(1)
    }

    private mutating func setByIndex(_ index: Int) -> Int {
        guard index != parent[index] else {
            return index
        }

        parent[index] = setByIndex(parent[index])

        return parent[index]
    }

    mutating func setOf(_ element: T) -> Int? {
        guard let indexOfElement = index[element] else {
            return nil
        }

        return setByIndex(indexOfElement)
    }

    mutating func inSameSet(_ aElement: T, _ bElement: T) -> Bool {
        guard let aSet = setOf(aElement), let bSet = setOf(bElement) else {
            return false
        }

        return aSet == bSet
    }

    mutating func unionSetsContaining(_ aElement: T, _ bElement: T) {
        guard let aSet = setOf(aElement), let bSet = setOf(bElement), aSet != bSet else {
            return
        }

        if size[aSet] < size[bSet] {
            parent[aSet] = bSet
            size[bSet] += size[aSet]
        } else {
            parent[bSet] = aSet
            size[aSet] += size[bSet]
        }
    }

}
