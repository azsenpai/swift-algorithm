
class BinarySearchTree<T: Comparable> {

    public var value: T
    public var parent: BinarySearchTree?
    public var left: BinarySearchTree?
    public var right: BinarySearchTree?

    init(value: T) {
        self.value = value
    }

    public convenience init(array: [T]) {
        precondition(!array.isEmpty)

        self.init(value: array.first!)

        for value in array.dropFirst() {
            insert(value)
        }
    }

    public var isRoot: Bool {
        return parent == nil
    }

    public var isLeaf: Bool {
        return left == nil && right == nil
    }

    public var isLeftChild: Bool {
        return parent?.left === self
    }

    public var isRightChild: Bool {
        return parent?.right === self
    }

    public var count: Int {
        return (left?.count ?? 0) + 1 + (right?.count ?? 0)
    }

    public var height: Int {
        return isLeaf
            ? 0
            : 1 + max(left?.height ?? 0, right?.height ?? 0)
    }

    public var depth: Int {
        var node = self
        var edges = 0

        while let parent = node.parent {
            node = parent
            edges += 1
        }

        return edges
    }

    public func insert(_ value: T) {
        if value < self.value {
            if let left = left {
                left.insert(value)
            } else {
                left = BinarySearchTree(value: value)
                left?.parent = self
            }
        } else {
            if let right = right {
                right.insert(value)
            } else {
                right = BinarySearchTree(value: value)
                right?.parent = self
            }
        }
    }

    public func minimum() -> BinarySearchTree {
        var node = self

        while let left = node.left {
            node = left
        }

        return node
    }

    public func maximum() -> BinarySearchTree {
        var node = self

        while let right = node.right {
            node = right
        }

        return node
    }

    public func remove() -> BinarySearchTree? {
        let node: BinarySearchTree?

        if let left = left {
            node = left.maximum()
        } else if let right = right {
            node = right.minimum()
        } else {
            node = nil
        }

        node?.remove()

        if let parent = parent {
            if isLeftChild {
                parent.left = node
            } else {
                parent.right = node
            }
        }

        node?.parent = parent
        node?.left = left
        node?.right = right

        left?.parent = node
        right?.parent = node

        parent = nil
        left = nil
        right = nil

        return node
    }

    public func search(value: T) -> BinarySearchTree? {
        if value < self.value {
            return left?.search(value: value)
        } else if value > self.value {
            return right?.search(value: value)
        } else {
            return self
        }
    }

    public func traverseInOrder(action: (T) -> Void) {
        left?.traverseInOrder(action: action)
        action(value)
        right?.traverseInOrder(action: action)
    }

    public func traversePreOrder(action: (T) -> Void) {
        action(value)
        left?.traversePreOrder(action: action)
        right?.traversePreOrder(action: action)
    }

    public func traversePostOrder(action: (T) -> Void) {
        left?.traversePostOrder(action: action)
        right?.traversePostOrder(action: action)
        action(value)
    }

    public func map(_ transform: (T) -> T) -> [T] {
        var result = [T]()

        if let left = left {
            result += left.map(transform)
        }

        result.append(transform(value))

        if let right = right {
            result += right.map(transform)
        }

        return result
    }

    public func toArray() -> [T] {
        return map { $0 }
    }

    public func predecessor() -> BinarySearchTree? {
        if let left = left {
            return left.maximum()
        }

        var node = self

        while let parent = node.parent {
            if parent.value < value {
                return parent
            }
            node = parent
        }

        return nil
    }

    public func successor() -> BinarySearchTree? {
        if let right = right {
            return right.minimum()
        }

        var node = self

        while let parent = node.parent {
            if parent.value > value {
                return parent
            }
            node = parent
        }

        return nil
    }

    public func isBST(minValue: T, maxValue: T) -> Bool {
        if value < minValue || value > maxValue {
            return false
        }

        let leftBST = left?.isBST(minValue: minValue, maxValue: value) ?? true
        let rightBST = right?.isBST(minValue: value, maxValue: maxValue) ?? true

        return leftBST && rightBST
    }

}

extension BinarySearchTree: CustomStringConvertible {

    var description: String {
        var result = ""

        if let left = left {
            result += "(\(left)) <- "
        }

        result += "\(value)"

        if let right = right {
            result += " -> (\(right))"
        }

        return result
    }

}

let tree = BinarySearchTree<Int>(value: 7)

tree.insert(2)
tree.insert(5)
tree.insert(10)
tree.insert(9)
tree.insert(1)

print(tree)

if let node9 = tree.search(value: 9) {
    print("node9 depth: \(node9.depth)")
}

print("traverseInOrder: ")
tree.traverseInOrder { value in print(value) }

print("traversePreOrder: ")
tree.traversePreOrder { value in print(value) }

print("traversePostOrder: ")
tree.traversePostOrder { value in print(value) }
