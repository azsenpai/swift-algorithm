
func kthLargest<T: Comparable>(_ array: [T], order k: Int) -> T? {
    guard k > 0 && k <= array.count else {
        return nil
    }

    var array = array
    let k = array.count - k

    func randomPivot(_ lower: Int, _ upper: Int) -> T {
        let pivotIndex = Int.random(in: lower...upper)
        array.swapAt(pivotIndex, upper)

        return array[upper]
    }

    func randomizedPartition(_ lower: Int, _ upper: Int) -> Int {
        let pivot = randomPivot(lower, upper)
        var i = lower

        for j in lower..<upper {
            if array[j] <= pivot {
                array.swapAt(i, j)
                i += 1
            }
        }

        array.swapAt(i, upper)

        return i
    }

    func randomizedSelect(_ lower: Int, _ upper: Int) -> T {
        guard lower < upper else {
            return array[lower]
        }

        let p = randomizedPartition(lower, upper)

        if k == p {
            return array[p]
        } else if k < p {
            return randomizedSelect(lower, p - 1)
        }

        return randomizedSelect(p + 1, upper)
    }

    return randomizedSelect(0, array.count - 1)
}

let array = [ 7, 92, 23, 9, -1, 0, 11, 6 ]

kthLargest(array, order: 1)
kthLargest(array, order: 2)
kthLargest(array, order: 3)
kthLargest(array, order: 4)
