
extension String {

    func indexOf(_ pattern: String) -> String.Index? {
        for i in self.indices {
            var textIndex = i
            var found = true

            for patternIndex in pattern.indices {
                if textIndex == self.endIndex || self[textIndex] != pattern[patternIndex] {
                    found = false
                    break
                }
                textIndex = self.index(after: textIndex)
            }

            if found {
                return i
            }
        }

        return nil
    }

}

let s = "Hello, World"
if let index = s.indexOf("World") {
    print(index.encodedOffset)
}

let animals = "🐶🐔🐷🐮🐱"
if let index = animals.indexOf("🐮") {
    print(index.encodedOffset)
}

