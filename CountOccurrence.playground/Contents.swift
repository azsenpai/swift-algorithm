
func isSorted<T: Comparable>(_ array: [T]) -> Bool {
    for i in 1..<array.count {
        if array[i] < array[i-1] {
            return false
        }
    }

    return true
}

func countOccurrencesOfKey<T: Comparable>(_ key: T, inArray array: [T]) -> Int {
    var leftBoundary: Int {
        var lower = 0
        var upper = array.count - 1

        while lower < upper {
            let middle = (lower + upper) / 2

            if key > array[middle] {
                lower = middle + 1
            } else {
                upper = middle
            }
        }

        return lower
    }

    var rightBoundary: Int {
        var lower = 0
        var upper = array.count - 1

        while lower < upper {
            let middle = (lower + upper + 1) / 2

            if key < array[middle] {
                upper = middle - 1
            } else {
                lower = middle
            }
        }

        return lower
    }

    let rightBoundaryIndex = rightBoundary

    guard key == array[rightBoundaryIndex] else {
        return 0
    }

    return rightBoundaryIndex - leftBoundary + 1
}

let array = [ 0, 1, 1, 3, 3, 3, 3, 6, 8, 10, 11, 11 ]

guard isSorted(array) else {
    fatalError("array must be sorted")
}

countOccurrencesOfKey(3, inArray: array)
