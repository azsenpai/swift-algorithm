
extension String {

    func indexOf(pattern: String) -> [Index] {
        var result = [Index]()

        func prefixFunc() -> [Index: Int] {
            var p = [Index: Int]()

            var i = pattern.startIndex
            p[i] = 0

            i = pattern.index(after: i)

            while i < pattern.endIndex {
                var j = p[pattern.index(before: i)] ?? 0

                while j > 0 && pattern[i] != pattern[pattern.index(pattern.startIndex, offsetBy: j)] {
                    j = p[pattern.index(pattern.startIndex, offsetBy: j - 1)] ?? 0
                }

                if pattern[i] == pattern[pattern.index(pattern.startIndex, offsetBy: j)] {
                    j += 1
                }

                p[i] = j
                i = pattern.index(after: i)
            }

            return p
        }

        var textIndex = self.startIndex
        var patternIndex = pattern.startIndex

        let p = prefixFunc()

        while textIndex < self.endIndex {
            if self[textIndex] == pattern[patternIndex] {
                textIndex = self.index(after: textIndex)
                patternIndex = pattern.index(after: patternIndex)

                if patternIndex == pattern.endIndex {
                    result.append(self.index(textIndex, offsetBy: -pattern.count))
                    patternIndex = pattern.startIndex
                }
            } else {
                if patternIndex != pattern.startIndex {
                    patternIndex = pattern.index(pattern.startIndex, offsetBy: p[pattern.index(before: patternIndex)] ?? 0)
                } else {
                    textIndex = self.index(after: textIndex)
                }
            }
        }

        return result
    }

}

let s = "💩🤫🤓💩💩"

for index in s.indexOf(pattern: "🤓💩") {
    print(s.distance(from: s.startIndex, to: index))
    // print(index.encodedOffset)
}
