
func merge<T: Comparable>(_ leftArray: [T], _ rightArray: [T]) -> [T] {
    var array = [T]()

    var leftIndex = 0
    var rightIndex = 0

    while leftIndex < leftArray.count && rightIndex < rightArray.count {
        if leftArray[leftIndex] < rightArray[rightIndex] {
            array.append(leftArray[leftIndex])
            leftIndex += 1
        } else if leftArray[leftIndex] > rightArray[rightIndex] {
            array.append(rightArray[rightIndex])
            rightIndex += 1
        } else {
            array.append(leftArray[leftIndex])
            leftIndex += 1

            array.append(rightArray[rightIndex])
            rightIndex += 1
        }
    }

    while leftIndex < leftArray.count {
        array.append(leftArray[leftIndex])
        leftIndex += 1
    }

    while rightIndex < rightArray.count {
        array.append(rightArray[rightIndex])
        rightIndex += 1
    }

    return array
}

func mergeSort<T: Comparable>(_ array: [T]) -> [T] {
    guard array.count > 1 else { return array }

    let middleIndex = array.count / 2

    let leftArray = mergeSort(Array(array[..<middleIndex]))
    let rightArray = mergeSort(Array(array[middleIndex...]))

    return merge(leftArray, rightArray)
}

let array = [2, 1, 5, 4, 9]
print(mergeSort(array))
