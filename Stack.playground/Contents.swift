
class Stack<T> {

    private var array = [T]()

    public var isEmpty: Bool {
        return array.isEmpty
    }

    public var count: Int {
        return array.count
    }

    public func push(_ newElement: T) {
        array.append(newElement)
    }

    public func pop() -> T? {
        return array.popLast()
    }

    public var top: T? {
        return array.last
    }

}

let stack = Stack<Int>()

stack.push(3)
print("top: \(stack.top)")

stack.push(7)
print("top: \(stack.top)")

print("pop: \(stack.pop())")
print("pop: \(stack.pop())")
