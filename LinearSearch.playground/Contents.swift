
func linearSearch<T: Equatable>(_ array: [T], key: T) -> Int? {
    for (i, value) in array.enumerated() {
        if key == value {
            return i
        }
    }

    return nil
}

let array = [5, 2, 4, 7]
linearSearch(array, key: 2)
