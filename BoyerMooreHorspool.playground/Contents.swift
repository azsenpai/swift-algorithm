
extension String {

    func indexOf(pattern: String) -> Index? {
        let patternCount = pattern.count

        guard patternCount > 0, patternCount <= self.count else {
            return nil
        }

        var skipTable = [Character: Int]()

        for (i, c) in pattern.enumerated() {
            skipTable[c] = patternCount - i - 1
        }

        var textIndex = index(startIndex, offsetBy: patternCount - 1)
        let patternLastIndex = pattern.index(before: pattern.endIndex)

        func backwards() -> Index? {
            var textIndex = textIndex
            var patternIndex = patternLastIndex

            while patternIndex > pattern.startIndex {
                textIndex = self.index(before: textIndex)
                patternIndex = pattern.index(before: patternIndex)

                if pattern[patternIndex] != self[textIndex] {
                    return nil
                }
            }

            return textIndex
        }

        while textIndex < self.endIndex {
            let c = self[textIndex]

            if c == pattern[patternLastIndex], let index = backwards() {
                return index
            }

            let offset = max(skipTable[c] ?? patternCount, 1)
            textIndex = self.index(textIndex, offsetBy: offset, limitedBy: self.endIndex) ?? self.endIndex
        }

        return nil
    }

}

let s = "Hello, World"
if let index = s.indexOf(pattern: "World") {
    print(index.encodedOffset)
}

let animals = "🐶🐔🐷🐮🐱"
if let index = animals.indexOf(pattern: "🐮") {
    print(index.encodedOffset)
}
