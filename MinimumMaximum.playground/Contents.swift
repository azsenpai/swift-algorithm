
func minimum<T: Comparable>(_ array: [T]) -> T? {
    guard var minElement = array.first else {
        return nil
    }

    for element in array.dropFirst() {
        if element < minElement {
            minElement = element
        }
    }

    return minElement
}

func maximum<T: Comparable>(_ array: [T]) -> T? {
    guard var maxElement = array.first else {
        return nil
    }

    for element in array.dropFirst() {
        if element > maxElement {
            maxElement = element
        }
    }

    return maxElement
}

func minimumMaximum<T: Comparable>(_ array: [T]) -> (minimum: T?, maximum: T?) {
    guard var minElement = array.first else {
        return (nil, nil)
    }

    var maxElement = minElement

    for element in array.dropFirst() {
        if element < minElement {
            minElement = element
        }
        if element > maxElement {
            maxElement = element
        }
    }

    return (minElement, maxElement)
}

let array = [ 8, 3, 9, 4, 6 ]

minimum(array)
maximum(array)

minimumMaximum(array)
